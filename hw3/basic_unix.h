//basic_unix.h
//Homework #3 Header File (UNIX)
//this file contains most of our functions outside of the initial class declaration.
//declared separately from the main header (traffic_declares) because I need the four traffic lights pre-declared in the header for it to work. And this separates.

void tick_tock()
{
	sleep(1);
	i_proc += 1;
	grn_proc += 1;
	yel_proc += 1;
	red_proc += 1;
	emve_proc += 1;

	if (emve_proc == 40)
		emergencyVehicle = 1;

}

void emergency_procedure()
{
		if (printed_warning == 0) {	//see end of this if statement for more info.
			cout << "EMERGENCY VEHICLE IN RANGE!\n"; //warning
			emve_proc = 0;
			printed_warning = 1; //this is kind of mandatory...
		}

		nord.emergency(); //sets light to "ER"
		sud.emergency(); //"ER"
		ost.emergency(); // "ER"
		west.emergency(); // "ER"

		tick_tock(); //tick tock goes the clock, even for the Doctor...

		cout << i_proc << "            " << nord.getDisplayColor() << "           " << sud.getDisplayColor() << "          " << ost.getDisplayColor() << "          " << west.getDisplayColor() << endl;

		if (emve_proc == 10) {
			emergencyVehicle = 0;
			nord.setDisplayColor(3);
			sud.setDisplayColor(3);
			ost.setDisplayColor(3);
			west.setDisplayColor(3);
			red_proc = 0;
			yel_proc = 0;
			grn_proc = 0;
		}

}

void initialize()
{

	rPointer = &red;
	yPointer = &yellow;
	gPointer = &green;

	i_proc = 0;
	red_proc = 0;
	yel_proc = 0;
	grn_proc = 0;

    //set names for each light
    nord.setName("North");
    sud.setName("South");
    ost.setName("East");
    west.setName("West");

    // Pre-set the colours of each light
    nord.setDisplayColor(1);
    nord_n = green;
    sud.setDisplayColor(1);
    sud_n = green;
    ost.setDisplayColor(3);
    ost_n = red;
    west.setDisplayColor(3);
    west_n = red;

    // Print out the current light configuration.

    cout <<"Seconds    North Light     South Light     East Light     West Light" << endl;
    cout <<"-------    -----------     -----------     ----------     ----------" << endl;

}

int chk()
{
	if (grn_proc == 9 ){
			if (nord_n == green)
			{
				nord.setDisplayColor(yellow);
				sud.setDisplayColor(yellow);

				nord_n = yellow;
				sud_n = yellow;

				yel_proc = 0;
				red_proc = 0;
			}

			if (ost_n == green)
			{
				ost.setDisplayColor(yellow);
				west.setDisplayColor(yellow);

				ost_n = yellow;
				west_n = yellow;

				yel_proc = 0;
				red_proc = 0;
			}
		}

	if (yel_proc == 3) {
			if (nord_n == yellow)
			{
				nord.setDisplayColor(red);
				sud.setDisplayColor(red);

				nord_n = red;
				sud_n = red;
				grn_proc = 0;

			}
			if (ost_n == yellow)
			{
				nord.setDisplayColor(red);
				sud.setDisplayColor(red);

				nord_n = red;
				sud_n = red;
				grn_proc = 0;
				red_proc = grn_proc;
			}
		}

}

int ns_red()
{
	if (red_proc == 5)
	{
			if (ost_n == red)
			{
				if (nord_n == red)
				{
				nord.setDisplayColor(green);
				sud.setDisplayColor(green);

				nord_n = green;
				sud_n = green;

				grn_proc = 0;
				yel_proc = 0;
				}
			}
	}
}

int ow_red()
{
	if (red_proc == 5)
	{
			if (nord_n == red)
			{
				if (ost_n == red)
				{
				ost.setDisplayColor(green);
				west.setDisplayColor(green);

				ost_n = green;
				west_n = green;

				grn_proc = 0;
				yel_proc = 0;
				}
			}
	}

}

    Blog Support P
