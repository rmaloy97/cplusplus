//candidates.h
///////
// This is where we'll create the array necessary to render the presidential candidates out.
///////
// Election Calculator (U.S.)
//
// Coded by James Maloy, all copyrights are retained by the original author.
//
////////

#define HEIGHT 2 //2 initially just for testing Carter and Reagan.
#define WIDTH 5 //Candidate Name, Political Party, Year of Run, Home State, Popular Vote total
