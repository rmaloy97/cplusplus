// bob's lawn service. bruh.
// bob_lawn.cpp
// "LEL RECTANGLES"

#include <iostream>
#include <stdio.h>

using namespace std;

class rectangleType
{
public:
  int length;
  int width;
  double fence_cost;
  double fertilizer_cost;
  int perimeter;
  int totalcost;
};

int processing_done;
int main()
{

  while (processing_done == 0)
    {

      rectangleType lawn_data;
      cout << "Please enter the length of the yard: \n";
	cin >> lawn_data.length;
      cout << "Please enter the width of the yard: \n";
	cin >> lawn_data.width;

      cout << "We inform you that as of March 30, 2016 -- the cost of fencing is approximately $10/ft; and fertilizer is $0.25 per foot.";
      lawn_data.fence_cost = 10.00;
      lawn_data.fertilizer_cost = 0.25;
      lawn_data.perimeter = (2 * lawn_data.length) + (2 * lawn_data.width);
      int area = lawn_data.length * lawn_data.width;
      cout << "Your yard's perimeter is: " << lawn_data.perimeter << "\n";
      cout << "Your yard's area is: " << area << "\n";
      lawn_data.totalcost = (lawn_data.fertilizer_cost * area) + (lawn_data.fence_cost * lawn_data.perimeter);
      cout << "\n We will require from you: " << lawn_data.totalcost << "\n";

      cout << "-----" << endl;
      processing_done = 1;
    }

  cout << "\n Thank you for choosing Bob's Lawn Care!\n";
  return 0;
}


