//	num_sqr_cube.cpp
// 	Numbers, Squares and Cubes

#include <iostream>
#include <string>
#include <cmath>

using namespace std;

double x;
double y;
double z;

int main()
{

cout << "Please enter the first number you wish to render: ";
cin >> y;

cout << "Please enter the last number you wish to render: ";
cin >> z;

cout << "number\t square\t cube\t" << endl;

	while (x <= z and x >= 0)
	{
		cout << x << "\t" << pow(x, 2) << "\t" << pow(x,3) << endl;
		x += 1;
	}

return 0;
}