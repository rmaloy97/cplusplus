/// "traffic_lights.cpp"
////////////////////////
/// COP2271 - Computer Programming 1
/// Homework #1 - The Four Way Stoplight Problem
/// Robert Maloy

#include <iostream>
#include <string>
#include "traffic.h"

int main()
{
	Traffic_Light nordlight; 	//	Northern light declaration
	Traffic_Light sudlight; 	// 	Southern light declaration
	Traffic_Light ostlight; 	// 	Eastern Light declaration
	Traffic_Light westlight; 	//  Western Light declaration
	
	cout << "North\t South\t East\t West\t" << endl;
	cout << "---------------------------------" << endl;
	nordlight.changeColor(3); 	//North is Red, should be light_status = 3
	sudlight.changeColor(3); 	//South is Red, should be light_status = 3
	ostlight.changeColor(1); 	//East is Green (almost had a Communist China joke there...)
	westlight.changeColor(1); 	// West is Green
	
	nordlight.StatusCheck(); 	//Status check!
	sudlight.StatusCheck(); 	//Status check!
	ostlight.StatusCheck(); 	//Status check!
	westlight.StatusCheck(); 	//Status check!

	cout << "" << endl;
	cout << "--------------------------------" << endl;
	
	ostlight.changeColor(3); //The East is Red, quick, someone call China!
	westlight.changeColor(3); //The West is Red? We're gonna need Joe McCarthy, stat!
	
	nordlight.StatusCheck(); 	//Status check!
	sudlight.StatusCheck(); 	//Status check!
	ostlight.StatusCheck(); 	//Status check!
	westlight.StatusCheck(); 	//Status check!
	
	cout << "" << endl;
	cout << "--------------------------------" << endl;
	
	nordlight.changeColor(1);
	
	nordlight.StatusCheck(); 	//Status check!
	sudlight.StatusCheck(); 	//Status check!
	ostlight.StatusCheck(); 	//Status check!
	westlight.StatusCheck(); 	//Status check!
	
	cout << "" << endl;
	cout << "--------------------------------" << endl;
	
	sudlight.changeColor(1);
	
	nordlight.StatusCheck(); 	//Status check!
	sudlight.StatusCheck(); 	//Status check!
	ostlight.StatusCheck(); 	//Status check!
	westlight.StatusCheck(); 	//Status check!
	
	cout << "" << endl;
	cout << "--------------------------------" << endl;
	
	sudlight.changeColor(3);
	nordlight.changeColor(3);
	
	nordlight.StatusCheck(); 	//Status check!
	sudlight.StatusCheck(); 	//Status check!
	ostlight.StatusCheck(); 	//Status check!
	westlight.StatusCheck(); 	//Status check!
	
	cout << "" << endl;
	cout << "--------------------------------" << endl;
	
	ostlight.changeColor(1);
	
	nordlight.StatusCheck(); 	//Status check!
	sudlight.StatusCheck(); 	//Status check!
	ostlight.StatusCheck(); 	//Status check!
	westlight.StatusCheck(); 	//Status check!
	
	cout << "" << endl;
	cout << "--------------------------------" << endl;
	
	westlight.changeColor(1);
	
	nordlight.StatusCheck(); 	//Status check!
	sudlight.StatusCheck(); 	//Status check!
	ostlight.StatusCheck(); 	//Status check!
	westlight.StatusCheck(); 	//Status check!
	
	cout << "" << endl;
	cout << "--------------------------------" << endl;
	
	westlight.changeColor(3);
	ostlight.changeColor(3);
	
	nordlight.StatusCheck(); 	//Status check!
	sudlight.StatusCheck(); 	//Status check!
	ostlight.StatusCheck(); 	//Status check!
	westlight.StatusCheck(); 	//Status check!
	
	cout << "" << endl;
	cout << "--------------------------------" << endl;	
	
  
	return 0;	
}