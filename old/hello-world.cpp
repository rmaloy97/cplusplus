// Title: Your Very 1st Ever C++ Program
// Description: Program to print some text on the screen
#include <iostream> // allows program to output data to the screen

// function main begins program execution
int main()
{
std::cout << "Hello World!\n"; // display message

return 0; // indicate that program ended successfully }
} //end program