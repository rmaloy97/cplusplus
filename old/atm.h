//atm.h

#include <iostream>
#include <string>

using namespace std;

class ATM {

private:
  int x; //denominations.
  int num_100s; //number of $100 we have
  int num_50s; //number of 50 bills
  int num_20s; //20 bills
  int num_10s; //Hamiltons
  int num_5s; //Lincolns
  int num_1s; //Washingtonians

public:

	int y; //number of bills loaded.
	int machineState_num; //0 = dev mode, 1 = customer mode
	int machineMax;
	int usrWithdraw;
	
	void pull()
	{
	
		cout << "Please enter the amount you wish to withdraw: ";
		cin >> usrWithdraw;
		
		if (usrWithdraw > 500)
		{
		
			cout << "Invalid transaction choice." << endl;		
		}
		
		if (usrWithdraw == 472)
		{
		
			cout << "Dispensing 4 $100s, 1 $50, 1 $20, and 2 $1s." << endl;
		}
		if (usrWithdraw == 500)
		{
		
			cout << "Dispensing 5 $100s." << endl;
		
		}
			
		if (usrWithdraw == 263)
		{
		
			cout << "Dispensing 1 $100, 3 $50s, 1 $10 and 3 $1s." << endl;
		}
		
		
	}

	void machineState(int machineState_num)
{

	if (machineState_num == 0)
	{
		cout << "System is in administrative mode." << endl;
	}

	if (machineState_num == 1)
	{
		cout << "System is online." << endl;
	}
}		

  void load(int x)
  {
      if (x == 100)
	{
	  cout << "Enter how many $" << x << " bills you wish to add: ";
	  cin >> y;
	  num_100s = y;
	}
	
	      if (x == 50)
	{
	  cout << "Enter how many $" << x << " bills you wish to add: ";
	  cin >> y;
	  num_50s = y;
	}
	
	      if (x == 20)
	{
	  cout << "Enter how many $" << x << " bills you wish to add: ";
	  cin >> y;
	  num_20s = y;
	}
	
	      if (x == 10)
	{
	  cout << "Enter how many $" << x << " bills you wish to add: ";
	  cin >> y;
	  num_10s = y;
	}
	
	      if (x == 5)
	{
	  cout << "Enter how many $" << x << " bills you wish to add: ";
	  cin >> y;
	  num_5s = y;
	}
	
	      if (x == 1)
	{
	  cout << "Enter how many $" << x << " bills you wish to add: ";
	  cin >> y;
	  num_1s = y;
	}

  }
  
  void num(int x)
  {
  
  	if (x == 100)
  	{
  		cout << num_100s << " x $100" << " = " << "\t" << num_100s * x << endl;
  	}
  	
  	if (x == 50)
  	{
  		cout << num_50s << " x $50" << " = " << "\t" << num_50s * x << endl;
  	}  	

  	if (x == 20)
  	{
  		cout << num_20s << " x $20" << " = " << "\t" <<  num_20s * x << endl;
  	}

  	if (x == 10)
  	{
  		cout << num_10s << " x $10" << " = " << "\t" << num_10s * x << endl;
  	}

  	if (x == 5)
  	{
  		cout << num_5s << " x $5" << " = " << "\t" << num_5s * x << endl;
  	}

  	if (x == 1)
  	{
  		cout << num_1s << " x $1" << " = " << "\t" << num_1s * x << endl;
  	}
  	
  	cout << " " << endl;
  }
  
void currentCashContent()
  {
  cout << "Total money: $" << (num_100s * 100) + (num_50s * 50) + (num_20s * 20) + (num_10s * 10) + (num_5s * 5) + (num_1s * 1);
  }


};