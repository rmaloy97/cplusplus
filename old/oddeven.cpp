/////////////////////////
////evenodd.cpp/////
/////////////////////////
// Date: January 20, 2016
// Title: "Determination - Even or Odd?"

#include <iostream>

using namespace std;

int main()
{

	int x;

	cout << "Enter a number, any number: "; //Enter number
	cin >> x;
	
	if ((x % 2) == 0) { //if there is no remainder...
	
		cout << x << " is an even number." << endl;
		
		}
		
	else { // if there is...
		
		cout << x << " is an odd number." << endl;
		
		}
		
	return 0; //program's over man

} //end main()