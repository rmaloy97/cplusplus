//checkbook.cpp
//// Checkbook program, Jan 25, 2016

#include <iostream>
using namespace std;

class Checkbook {

	int bankTotal;
	int withdrawal;
	int deposit;

public:
	void depositMoney(int deposit)
	{
		bankTotal += deposit;
	}
	
	void withdrawMoney(int withdrawal)
	{		
		if (bankTotal < withdrawal) {
		
			cout << "You can't do that, you'd overdraft!";
			withdrawMoney(0);
		}
		
		else {
		
		bankTotal -= withdrawal;
		
		}
	
	}
	
	void CheckmyBalance()
	{
	
		cout << "$" << bankTotal << endl;
	
	}

};

int main()
{

	int userChoice;
	
	Checkbook userBankAcct;	
	cout << "Thank you for signing up for a new account! Your account has been credited $100!" << endl;
	userBankAcct.depositMoney(100);
	userBankAcct.CheckmyBalance();
	cout << "Please enter the amount you wish to withdraw: ";
	cin >> userChoice;
	userBankAcct.withdrawMoney(userChoice);
	cout << "\n";
	userBankAcct.CheckmyBalance();
	cout << "Please enter the amount you wish to withdraw: ";
	cin >> userChoice;
	userBankAcct.withdrawMoney(userChoice);
	cout << "\n";
	userBankAcct.CheckmyBalance();
	
}