/////////////////////////
////getting_old.cpp/////
/////////////////////////
// Date: January 11, 2016
// Title: "Introduction to In-Class Programming Assignment" -- Class 2
// Program will calculate how many days I have been alive.

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
	int age;
	
	cout << "Please enter your current age (in years): ";
	cin >> age;
	
	cout << "You have been alive for " << age << " years" << endl;
	cout << "This translates to " << age * 365 << " days assuming today was your 19th birthday." << endl;
	cout << "You have approximately " << (120-age)*365 << " days remaining of your life." << endl;
	
	return 0;
	
}