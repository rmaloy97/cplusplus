/////////////////////////
////lotto.cpp/////
/////////////////////////
// Date: January 20, 2016
// Title: "Did you win the Powerball?"

#include <iostream>

using namespace std;

int main()
{
	enum WinningNumbers { a = 1, b = 4};
	
	int userNum_1;
	int userNum_2;
	int userNum_3;
	int userNum_4;
	int winningCombinations = 0;
	
	cout << "Please enter your four lottery number choices (1-10): ";
	cin >> userNum_1;
	cout << "2nd Number: ";
	cin >> userNum_2;
	cout << "3rd Number: ";
	cin >> userNum_3;
	cout << "4th Number: ";
	cin >> userNum_4;
	cout << " " << endl;
	
	if (userNum_1 >= a && userNum_1 <= b) {
		cout << "Number matched!" << endl;
		cout << " " << endl;
		winningCombinations += 1;
		}
	if (userNum_2 >= a && userNum_2 <= b) {
		cout << "Number matched!" << endl;
		cout << " " << endl;
		winningCombinations += 1;
		}
	if (userNum_3 >= a && userNum_3 <= b) {
		cout << "Number matched!" << endl;
		cout << " " << endl;
		winningCombinations += 1;
		}
	if (userNum_4 >= a && userNum_4 <= b) {
		cout << "Number matched!" << endl;
		cout << " " << endl;
		winningCombinations += 1;
		}
		
	cout << "User-chosen numbers: " << userNum_1 << ", " << userNum_2 << ", " << userNum_3 << ", " << userNum_4 << endl;
	cout << "Winning numbers: " << a << ", " << a+1 << ", " << a+2 << ", " << b << endl;
	cout << "You have " << winningCombinations << " matching numbers." << endl;
	
	if (winningCombinations == 1) {
		cout << "You win $1!";
	}
	
	if (winningCombinations == 2) {
		cout << "You win $11!";
	}
	
	if (winningCombinations == 3) {
		cout << "You win $111!";
	}
	
	if (winningCombinations == 4) {
		cout << "You win $1,111!";
	}
	
	return 0;
	
}