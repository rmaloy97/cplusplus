// File Name: hw4.cpp
// Title: HW2 -- but different this time!
// Robert Maloy
// "So, the third one was bunk, now let's BACKTRACK!"

#include "hw4.h"
#include "basic.h"
#include "data_grab.h"

int main()
{
  initialize(); //check initialize function -- basic.h
  data_acquisition(); //grabs data from file and stuffs it into a vector.

// debugging stuff; just to see if this stuff works as designed. :)

//    while (i_proc < last_line_read)
//    {
//        cout << orientation[i_proc] << " ";
//        cout << arrival_time[i_proc] << endl;
//        Sleep(200);
//        i_proc += 1;
//    }

    while (i_proc < 240 )
    {
//        while (emergencyVehicle == 1)
//        {
//            emergency_procedure();
//        }

        tick_tock(); //tick tock, the time is being kept.
        Waitlist(); //checking the waitlist stuff before continuing.

        //below are checks
        chk();
        ns_red();
        ow_red();

        //(debug, checks waitlists)
        //cout << i_proc << "            " << ns_waitlist << "            " << ow_waitlist << "\n";
        //cout << nsw << " " << oww << endl;
        //(debug, checks color timers)
        //cout << i_proc << "            " << red_proc << "            " << yel_proc << "            " << grn_proc << "            " << emve_proc << endl;
		//(release, displays colors.)
		cout << i_proc << "            " << nord.getDisplayColor() << "           " << sud.getDisplayColor() << "          " << ost.getDisplayColor() << "          " << west.getDisplayColor() << endl;
    }

    cout << "\n";
    cout << "Simulation complete. Data will render once you hit any key.\n";
    system("pause"); //Win32 command -- "pause" sends the "Hit any key to continue..." line.
    system("cls"); //this will allow us to clear the data off the screen at the conclusion of the program, and allow for our "post-game" data (averages, lines, etc.)
    //this is where our "post-game" data will display.
    Total('N');
    Total('S');
    Total('E');
    Total('W');
    cout << "\n";
    cout << "Longest line: " << max_list;
    if (ns_max == 1)
        cout << " (along North-South Axis)\n";
    if (ow_max == 1)
        cout << " (along East-West Axis)\n";

    cout << "\n";

    cout << "Average time to get through light (N/S): " << Average('N') << " seconds" << endl;
    cout << "Average time to get through light (E/W): " << Average('E') << " seconds" << endl;

    return 1;
}
