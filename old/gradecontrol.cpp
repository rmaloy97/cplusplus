// gradecontrol.cpp

#include <iostream>
#include <string>
using namespace std;

int main()
{

	double sum = 0.00;
	double quizGrade[10];
	int n = 1;
	cout << "Please enter the quiz grades you wish to compile and average:" << endl;

	while (n <= 10) {

		cout << "Quiz #" << n << ": ";
		cin >> quizGrade[n];
		sum = sum + quizGrade[n];
		n += 1;
	}
	
	n = 1;
	cout << "Test #\t Grade\t" << endl;
	cout << "----------------" << endl;

	while (n <= 10) {
	cout << n << "\t" << quizGrade[n] << "%" << endl;
	n += 1;
	}
	
	cout << "----------------" << endl;
	cout << "Average: " << sum/10 << "%" << endl;
	
	return 0;
}