//atm.cpp

#include "atm.h"

int main()
{

	ATM newATM;
	newATM.machineState(0);

	newATM.load(100);
	newATM.load(50);
	newATM.load(20);
	newATM.load(10);
	newATM.load(5);
	newATM.load(1);
	
	cout << "Current bills in ATM:" << endl;
  	cout << "---------------------" << endl;
	newATM.num(100);
	newATM.num(50);
	newATM.num(20);
	newATM.num(10);
	newATM.num(5);
	newATM.num(1);
	
	newATM.currentCashContent();
	cout << " " << endl;
	newATM.machineState(1);
	
	newATM.pull();

	return 0;

}