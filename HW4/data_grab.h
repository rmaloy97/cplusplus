//data_grab.h

//below are our includes for the data controller.
#include <string>
#include <fstream>
#include <cstdlib>
#include <sstream>
//vectors
vector <char> orientation;
vector <int> arrival_time;

void data_acquisition() //"Data Acquisition".
{
  ifstream indata; //"input stream named indata"
  int timeValue; //This is how we note the seconds.
  string restOfLine; //rest of the line.
  char direction; //cardinal direction

  indata.open("HW #4 Data.txt");  // Open HW #4 data file

  while (!indata.eof()) //while we are NOT at the end of file.
    {
        indata.get(direction);
        orientation.push_back(direction);
        getline(indata,restOfLine);
        istringstream iss(restOfLine);
        iss >> timeValue;

        arrival_time.push_back(timeValue);

        last_line_read += 1;
    }
    indata.close(); //no need to keep it in memory now that it's stored in vectors.
}

void Total(char chosen_direction)
{
 x = 0;
 while (x < last_line_read)
 {
    if ( orientation[x] == chosen_direction)
    {
        car_total += 1;
    }
    x++;
}

    if (chosen_direction == 'N')
        cout << "Northbound cars: ";

    if (chosen_direction == 'S')
        cout << "Southbound cars: ";

    if (chosen_direction == 'E')
        cout << "Eastbound cars: ";

    if (chosen_direction == 'W')
        cout << "Westbound cars: ";

    cout << car_total << endl;
    x = 0;
    car_total = 0;
}

void Waitlist()
{
    x = 0;
    while (x < last_line_read)
    {
        if (max_list < ns_waitlist)
            max_list = ns_waitlist;
            ns_max = 1;
            ow_max = 0;

        if (max_list < ow_waitlist)
            max_list = ow_waitlist;
            ow_max = 1;
            ns_max = 0;

        if (nord_n == red || nord_n == yellow || nord_n == 4)
        {
            if (arrival_time[x] == i_proc)
            {
                if (orientation[x] == 'N')
                 {
                    ns_waitlist += 1;
                 }

                if (orientation[x] == 'S')
                {
                    ns_waitlist += 1;
                }
            }
        }

        if (ost_n == red || ost_n == yellow || ost_n == 4)
        {
            if (arrival_time[x] == i_proc)
            {
                if (orientation[x] == 'E')
                    ow_waitlist += 1;
                if (orientation[x] == 'W')
                    ow_waitlist += 1;
            }
        }
        x += 1;
    }
}

double Average(char choice)
{
        int n;
        double return_value = 0.0;
        if (choice == 'N')
            n = ns_wait_time.size();

        if (choice == 'E')
            n = ow_wait_time.size();

        for ( int i=0; i < n; i++)
        {
            if (choice == 'N')
                return_value += ns_wait_time[i];

            if (choice == 'E')
                return_value += ow_wait_time[i];
        }

        return ( return_value / last_line_read);
}


