//vector_float.cpp
// Bleh.

#include <iostream>
#include <stdio.h>
#include <vector>
#include <cmath>

using namespace std;

float sum_of_elements = 0;
int vector_sizes = 25;

int main()
{

	vector<float> first_vector (25,3.3684);
		
	for (int i = 0; i < first_vector.size(); i++)
	{
	
		cout << first_vector[i] << "\n";
	}
	
	cout << "\n";
	cout << "First Vector complete. Initializing second...\n";
	cout << " " << endl;
	
	vector<float> second_vector (25,3.4859);
	vector<float> third_vector (25,1.414);
	vector<float> fourth_vector;
	
	for (int i = 0; i < second_vector.size(); i++)
	{
		cout << second_vector[i] << ", ";
		sum_of_elements += second_vector[i];
		cout << third_vector[i] << ", ";
		sum_of_elements += third_vector[i];
		fourth_vector.push_back(sum_of_elements);
		sum_of_elements = 0;
		cout << fourth_vector[i] << ".\n";
	
	}
	
	cout << "Second set of vectors complete. Moving to final stage...\n";
	
	vector<float> fifth_vector(second_vector);
	
	for (int i = 0; i < vector_sizes;)
	{
	
		cout << fifth_vector[i] << ", ";
		float float_num = pow(fifth_vector[i],2);
		fifth_vector.insert (fifth_vector.begin(), float_num);
		cout << fifth_vector[i] << ". \n";
		i += 1;	
	
	}	
}