/// gradebook.cpp
///////
/// A functional program that creates a class gradebook; at least, the member function displayMessage.

#include <iostream>
using namespace std;

//Defines class GradeBook
class GradeBook
{
public:
	void displayMessage()
	{
		cout << "Welcome to the GradeBook!" << endl;
	} // end function DisplayMessage
}; // End class GradeBook

int main()
{
	GradeBook myGradeBook; //create a GradeBook
	myGradeBook.displayMessage(); //Call object's displayMessage function
}
// program concludes