//progasm_classes.h

class Students
{

	private:
		int midtermGrade;
		int homeworkGrade;
		string student_FirstName;
		string student_LastName;
		
	public:
	
		void setName()
		{
			cout << "Please enter student first name: ";
			cin >> student_FirstName;
			cout << "Please enter student last name: ";
			cin >> student_LastName;
		}
		
		void getName()
		{
			cout << "Student Name: " << student_LastName << ", " << student_FirstName << endl;
		}
		
		void setMidtermGrade()
		{
		
			cout << "Please enter the % of the student's midterm grade: ";
			cin >> midtermGrade;
		}
		
		void getMidtermGrade()
		{
			cout << "Midterm Grade: " << midtermGrade << "%" << endl;
		}
		
		void setHomeworkGrade()
		{
			cout << "Please enter the % of the student's homework grade: ";
			cin >> homeworkGrade;
		}
		
		void getHomeworkGrade()
		{
			cout << "Homework Grade: " << homeworkGrade << "%" << endl;
		}
};