using namespace std; //using the standard namespace.

//variable declaration

class Traffic_Light { //Class: Traffic Light

private:
	int light_status; //Status of the light
	int setNum; //Number we will set the light to.

public:
	
	void changeColor(int setNum)
	{ 
		light_status = setNum; //Light Status is now what number you define it as.
	}

	void StatusCheck()
	{
		if (light_status == 1)
		{
			cout << "  " << "Green";
		}
		
		if (light_status == 2)
		{	
			cout << "  " << "Yellow";
		}
	
		if (light_status == 3)
		{
			cout << "  " << "Red";
		}		
	}
};