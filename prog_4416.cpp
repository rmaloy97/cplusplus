// Programming exercise -- April 4th, 2016
// 'bleh.'

#include <iostream>
#include <stdio.h>

using namespace std;

class BankAccount
{

public:
  int account_id;
  double checking_balance;
  double minimum_bal;  
  double savings_balance;
  int deposit;
  int withdraw;
  int x;
  int check;

  void set_id()
  {
    cout << "Please enter new User ID: ";
    cin >> x;
    account_id = x;
    cout << "\n";
  }

  void grab_id()
  {
    cout << "Account ID: " << account_id << endl;
  }

  void grab_balance()
  {
    cout << "$" << savings_balance << " $" << checking_balance << endl;
}

  void print_information()
  {
    cout << "ID: " << account_id << "\n";
    cout << "Savings Balance: " << "$" << savings_balance << "\n";
    cout << "Checking Balance: " << "$" << checking_balance << "\n";
    cout << "-----------\n";

  }
  
  void minimum_check()
  {
  	minimum_bal = 5;
  
    if (checking_balance < minimum_bal)
      cout << "You have insufficient funds. Withdraw from Checking is not permitted.\n";
      
    if (savings_balance < minimum_bal)
    	cout << "You have insufficient funds. Withdraw from Savings is not permitted.\n";
    	
    if (savings_balance < minimum_bal && checking_balance < minimum_bal)
    	cout << "You have insufficient funds. You may not withdraw any cash.\n";

    else
      check = 1;
  }
  
};

class checkingAccount: public BankAccount
{
private:
  
void deposit_cash()
  {

    cout << "Please enter deposit amount: ";
    cin >> deposit;

  }

void withdraw_cash()
  {
    cout << "Please enter withdrawal amount: ";
    cin >> withdraw;

  }
};

class savingsAccount: public BankAccount
{
  
  void deposit_cash()
  {

    cout << "Please enter deposit amount: ";
    cin >> deposit;

  }

  void withdraw_cash()
  {
    cout << "Please enter withdrawal amount: ";
    cin >> withdraw;

  }  

};


int main()
{

  checkingAccount user1_c;
  savingsAccount user1_s;
  
 	user1_c.set_id();
 	user1_c.print_information();

  return 0;
}
