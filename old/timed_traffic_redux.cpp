//timed_traffic_redux.cpp
//Homework #2: Something something timed traffic lights.
//Programmer humour: You know who would have an easier time programming this? A dirty synth.

#include "traffic_redux.h"

	void breakTime( int seconds )
	{
		clock_t temp;
		temp = clock () + seconds * CLOCKS_PER_SEC ;
		while (clock() < temp) {}		
	}

int main()
{
	int x;
	
	//constructing the time keeping methods
	time_t start; //"start" is a variable that keeps time.
	start = time(0); //the current time is our base-point; not 1 January 1970.
	double seconds_passed = difftime( time(NULL), start ); //seconds passed is the difference between "start" and now.
	//this should work... maybe? idk.
	
	cout << start << endl;
	
	for (x=10; x>0; x--) 
	{ 
		cout << endl;
	}
	
	cout << seconds_passed << " seconds passed" << endl;

	return 0;
}