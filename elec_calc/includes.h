//includes.h
///////
// This is where our functions and includes go.
///////
// Election Calculator (U.S.)
//
// Coded by James Maloy, all copyrights are retained by the original author.
//
////////

using namespace std;
//we're including a lot of these just for posterity. Who knows if we'll need them! Ha!
#include <iostream> //i/o stream
#include <conio.h> //conditional i/o
#include <stdio.h> //standard in and out
#include <windows.h> //win32 calls in case they're needed.
#include <fstream> //file stream
#include <sstream> //sstream
#include <vector>
#include <algorithm>
#include <deque>

//Program Metadata
int prog_MajorVersion = 0;
int prog_MinorVersion = 1;
int prog_BuildNum = 126;
string prog_BetaStage = "Milestone 1";


