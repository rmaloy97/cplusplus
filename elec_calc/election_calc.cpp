//election_calc.cpp
///////
// Election Calculator (U.S.)
//
// Coded by James Maloy, all copyrights are retained by the original author.
//
////////

#include "includes.h"
#include "candidates.h"

int i = 0;

int main()
{

    system("cls");
    cout << "Election Calculator\n";
    cout << "Version " << prog_MajorVersion << "." << prog_MinorVersion << " r" << prog_BuildNum << " (" << prog_BetaStage << ") \n";
    cout << "-------------------------------------------------------------------------------------------------------------" << endl;
    cout << "NOTICE: This program is designed to only be used to properly calculate a United States presidential election.\n";
    cout << "-------------------------------------------------------------------------------------------------------------" << endl;
    cout << "Candidate                          Year Ran                     Home State                       Popular Vote" << endl;
    cout << "-------------------------------------------------------------------------------------------------------------" << endl;

    if (i <= 4, i++)
        cout << POTUS_CANDIDATE_LIST[i] << endl;

    return 0;
}
