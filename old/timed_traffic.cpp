//timed_traffic.cpp

#include "traffic.h"
#include <ctime>

	void breakTime( int seconds )
	{
		clock_t temp;
		temp = clock () + seconds * CLOCKS_PER_SEC ;
		while (clock() < temp) {}		
	}

int main()
{
	//constructing the time keeping methods
	time_t now = time(0);
	time_t seconds = time(NULL);
	struct tm moment;
	double seconds_passed = difftime(now, seconds);
	//this should work... maybe? idk.
	
	Traffic_Light nordlight; 	//	Northern light declaration
	Traffic_Light sudlight; 	// 	Southern light declaration
	Traffic_Light ostlight; 	// 	Eastern Light declaration
	Traffic_Light westlight; 	//  Western Light declaration
	
	//setting defaults
	cout << "Seconds\t North\t South\t East\t West\t" << endl;
	cout << "--------------------------------------------" << endl;
	nordlight.changeColor(1); //only one that's green
	sudlight.changeColor(3);
	ostlight.changeColor(3);
	westlight.changeColor(3);
	
	// ------------------------------
	cout << seconds_passed << "\t";
	nordlight.StatusCheck(); 	//Status check!
	sudlight.StatusCheck(); 	//Status check!
	ostlight.StatusCheck(); 	//Status check!
	westlight.StatusCheck(); 	//Status check!

	cout << "" << endl;
	cout << "--------------------------------------------" << endl;
	
	ostlight.changeColor(3); //The East is Red, quick, someone call China!
	westlight.changeColor(3); //The West is Red? We're gonna need Joe McCarthy, stat!
	
	cout << seconds_passed << "\t";
	nordlight.StatusCheck(); 	//Status check!
	sudlight.StatusCheck(); 	//Status check!
	ostlight.StatusCheck(); 	//Status check!
	westlight.StatusCheck(); 	//Status check!
	
	cout << "" << endl;
	cout << "--------------------------------------------" << endl;
	
	nordlight.changeColor(1);
	
	cout << seconds_passed << "\t";
	nordlight.StatusCheck(); 	//Status check!
	sudlight.StatusCheck(); 	//Status check!
	ostlight.StatusCheck(); 	//Status check!
	westlight.StatusCheck(); 	//Status check!
	
	cout << "" << endl;
	cout << "--------------------------------------------" << endl;
	
	sudlight.changeColor(1);
	
	cout << seconds_passed << "\t";
	nordlight.StatusCheck(); 	//Status check!
	sudlight.StatusCheck(); 	//Status check!
	ostlight.StatusCheck(); 	//Status check!
	westlight.StatusCheck(); 	//Status check!
	
	cout << "" << endl;
	cout << "--------------------------------------------" << endl;
	
	sudlight.changeColor(3);
	nordlight.changeColor(3);
	
	cout << seconds_passed << "\t";
	nordlight.StatusCheck(); 	//Status check!
	sudlight.StatusCheck(); 	//Status check!
	ostlight.StatusCheck(); 	//Status check!
	westlight.StatusCheck(); 	//Status check!
	
	cout << "" << endl;
	cout << "--------------------------------------------" << endl;
	
	ostlight.changeColor(1);
	
	cout << seconds_passed << "\t";
	nordlight.StatusCheck(); 	//Status check!
	sudlight.StatusCheck(); 	//Status check!
	ostlight.StatusCheck(); 	//Status check!
	westlight.StatusCheck(); 	//Status check!
	
	cout << "" << endl;
	cout << "--------------------------------------------" << endl;
	
	westlight.changeColor(1);
	
	cout << seconds_passed << "\t";
	nordlight.StatusCheck(); 	//Status check!
	sudlight.StatusCheck(); 	//Status check!
	ostlight.StatusCheck(); 	//Status check!
	westlight.StatusCheck(); 	//Status check!
	
	cout << "" << endl;
	cout << "--------------------------------------------" << endl;
	
	westlight.changeColor(3);
	ostlight.changeColor(3);
	
	cout << seconds_passed << "\t";
	nordlight.StatusCheck(); 	//Status check!
	sudlight.StatusCheck(); 	//Status check!
	ostlight.StatusCheck(); 	//Status check!
	westlight.StatusCheck(); 	//Status check!
	
	cout << "" << endl;
	cout << "--------------------------------------------" << endl;	
	
  
	return 0;	
}