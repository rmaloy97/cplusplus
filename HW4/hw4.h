//hw4.h
//Homework #4 Header File

#include <iostream> //i/o stream
#include <ctime> //time functions
#include <stdio.h> //standard in and out
#include <conio.h> //conditional io
#include <windows.h> //win32 api
#include <math.h>
#include <cmath>
#include <vector>
using namespace std;

vector <int> ns_wait_time;
vector <int> ow_wait_time;

//start declarations
int i_proc, grn_proc, yel_proc, red_proc, emergencyVehicle, emve_proc, printed_warning, CheckSync; //seconds
int green = 1; //number to indicate we have a green
int yellow = 2; //number to indicate yellow
int red = 3; // num to indicate red
int last_line_read = 0; //fundamental in setting where the program runs to.
int nord_n, sud_n, ost_n, west_n; //some numbers to keep track of junk
int average, car_total; //average wait-time
int x, y, ns_waitlist, ow_waitlist, max_list, ns_max, ow_max; //x is a variable of massive use.
int nsw, oww, w;

class Traffic_Light
{

    public:

    //Constructors
    Traffic_Light ()
    {
          currentLightColor = "";
          lightName = "";
    };
    //stop constructors

    void setName(string name) //names can be set at one's desire.
    {
        lightName = name;
    }

    string getName() //"Who am I?" certainly not Jean ValJean...
    {
       return(lightName);
    }

    void setDisplayColor(int ChosenNum)
    {
    	if (ChosenNum == 1)
    	{
       		currentLightColor = "Green";
       	}

       	if (ChosenNum == 2)
       	{
       		currentLightColor = "Yellow";
       	}

       	if (ChosenNum == 3)
       	{
       		currentLightColor = "Red";
       	}

       	if (ChosenNum == 4)
       		currentLightColor = "ER";
    }

    string getDisplayColor() //what is the color?
    {
       if (currentLightColor == "Red")
          return(currentLightColor+"  ");
       else
        return(currentLightColor);
    }

    int emergency()
    {
        setDisplayColor(4); //"emergency red" -- a flashing red light; this tells drivers that there is an emergency vehicle approaching.
    }

private:

      string currentLightColor;
      string lightName;

}; // class is over!

Traffic_Light nord;
Traffic_Light sud;
Traffic_Light ost;
Traffic_Light west;
