/////////////////////////
////largerequal.cpp/////
/////////////////////////
// Date: January 20, 2016
// Title: "Is this larger or equal?"

#include <iostream>
using namespace std;

int main() {

	int number1;
	int number2;
	
	cout << "Please enter the first number: ";
	cin >> number1;
	cout << "Please enter the second number: ";
	cin >> number2;
	
	if (number1 > number2) {
		cout << number1 << " is greater than " << number2 << endl;
	}
	
	if (number1 < number2) {
		cout << number2 << " is greater than " << number1 << endl;
	}
	
	if (number1 == number2) {
		cout << number1 << " and " << number2 << " are equal." << endl;
	}


	return 0;
}
