///In-class C++ Programming Assignment #1
///Robert Maloy

#include "progasm_header.h"
#include "progasm_classes.h"

int main()
{

	Students Student1;
	Student1.setName();
	Student1.setHomeworkGrade();
	Student1.setMidtermGrade();
	
	Student1.getName();
	Student1.getHomeworkGrade();
	Student1.getMidtermGrade();
	return 0;
}