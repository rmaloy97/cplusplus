// File Name: basic_win32.cpp
// Title: The Four-Way Stoplight Problem (now with more complex logic)
// Robert Maloy
// "Follow along and you'll find rich rewards."

#include "traffic_declares.h"
#include "basic_win32.h" //basic class and some functions that can be removed from the system

int main()
{

	initialize();

    while (i_proc < last_line_read ) //this number may be modified to lengthen process time. Currently set to 90 seconds (or 1 minute, 30 seconds) for testing purposes.
    {

    	while (emergencyVehicle == 1)
    	{
    		emergency_procedure();
    	}

        tick_tock(); //tick tock, the time is being kept.

        //below are checks
        nord_sud_chk();
        ost_west_chk();

        ow_red();
    	ns_red();


		cout << i_proc << "            " << nord.getDisplayColor() << "           " << sud.getDisplayColor() << "          " << ost.getDisplayColor() << "          " << west.getDisplayColor() << endl;
    }

}
